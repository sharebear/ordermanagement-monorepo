# ordermanagemen-monorepo

This repository is for testing out CI techniques for a monorepo that can be
deployed to an OpenShift/Kubernetes cluster.

The application itself is a small application that was thrown together as
part if a job interview in June 2017, the original separate repositories are
still available on GitHub at the following URLs

* https://github.com/sharebear/ordermanagement-api
* https://github.com/sharebear/ordermanagement-ui
* https://github.com/sharebear/ordermanagement-terraform

## Goals

* Only build sub-paths that need to be built
* Investigate if this can be done sanely with git-flow
* Build a "configuration" container encapsulating deploy commands
* Demonstrate use of common base images both for apps and for pipeline steps
* ... moar to come

